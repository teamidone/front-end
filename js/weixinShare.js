/*
 * ajax获取微信授权
 */
$.ajax({
	url: sw.baseHref + 'api/h5/tableInfo/ticket',
	type: 'post',
	cache: false,
	dataType: 'json',
	success: function(item) {
		if (item.code == 1000) {
			sw.cookie.setCookie("ticket",item.data);
		}
	}
});
/**
 * 朋友圈、朋友、qq分享
 * @param {Object} ticket 
 */
	$.post(sw.baseHref + 'api/h5/tableInfo/sign', {
		url: encodeURI(location.href.split('#')[0]),
		ticket: sw.cookie.getCookie("ticket")
	},
	function(item) {
		if (item.code == 1000) {
			wx.config({
				debug: false,
				appId: item.data.appId,
				timestamp: item.data.timestamp,
				nonceStr: item.data.nonceStr,
				signature: item.data.signature,
				jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareQZone']
			});
			wx.ready(function() {
				//分享到朋友圈
				wx.onMenuShareTimeline({
					title: document.getElementsByName('data-sw-title')[0].content,
					desc: document.getElementsByName('data-sw-content')[0].content, // 分享描述
					link: encodeURI(location.href.split('#')[0]), // 分享链接
					imgUrl: sw.baseHref + document.getElementsByName('data-sw-icon')[0].content, // 分享图标
					success: function() {
						//alert('timeline success');
					}
				});
				//分享给朋友
				wx.onMenuShareAppMessage({
					title: document.getElementsByName('data-sw-title')[0].content, // 分享标题
					desc: document.getElementsByName('data-sw-content')[0].content, // 分享描述
					link: encodeURI(location.href.split('#')[0]), // 分享链接
					imgUrl: sw.baseHref + document.getElementsByName('data-sw-icon')[0].content, // 分享图标
					type: '', // 分享类型,music、video或link，不填默认为link
					dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
					success: function() {
						// 用户确认分享后执行的回调函数
						//alert("success");
					},
					fail: function() {
						//alert("fail");
					},
					cancel: function() {
						// 用户取消分享后执行的回调函数
						//alert("cancel");
					}
				});
				//分享到QQ
				wx.onMenuShareQQ({
					title: document.getElementsByName('data-sw-title')[0].content, // 分享标题, // 分享标题
					desc: document.getElementsByName('data-sw-content')[0].content, // 分享描述, // 分享描述
					link: encodeURI(location.href.split('#')[0]), // 分享链接
					imgUrl: sw.baseHref + document.getElementsByName('data-sw-icon')[0].content, // 分享图标
					success: function() {
						// 用户确认分享后执行的回调函数
					},
					cancel: function() {
						// 用户取消分享后执行的回调函数
					}
				});
			});
			wx.error(function(res) {
				console.log("error:" + res);
			});
		} else {
			alert("客官，等一会儿，店小儿不在…");
		}
	});
