(function(window, document) {
	//扫位全局变量
	window.sw = {
		baseHref: "http://m.w5t5.com/",
		elements: {}
	};
	/**
	 * 初始化
	 * @param page_width 页面最大宽度
	 */
	window.sw.initPage = function(page_width) {
			page_width = page_width ? page_width : 750;
			var _self = {};
			_self.width = page_width; //设置默认最大宽度
			_self.fontSize = 100; //默认字体大小
			_self.ratio = 320 / page_width;
			_self.widthProportion = function() {
				var p = window.innerWidth / _self.width;
				if (p > 1) {
					return 1;
				}

				if (p < _self.ratio) {
					return _self.ratio;
				}
				return p;
			};
			_self.changePage = function() {
				document.documentElement.style.fontSize = _self.widthProportion() * _self.fontSize + 'px';
			};
			_self.changePage();
			window.addEventListener("resize", function() {
				_self.changePage();
			}, false);
		}
	/**
	 * cookie 设置、获取、删除
	 */
	window.sw.cookie = (function() {
		/**
		 * 设置cookie
		 * @param name
		 * @param value
		 * @param expires 到期时间，单位秒
		 * @param path
		 */
		function setCookie(name, value, expires, path) {

			var expdate = new Date();

			expdate.setTime(expdate.getTime() + (expires * 1000));

			document.cookie = name + "=" + escape(value) + ";expires=" + expdate.toGMTString() + (path ? ";path=" + path : "");
		}


		function getCookie(name) {

			var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");

			if (arr = document.cookie.match(reg))

				return unescape(arr[2]);
			else
				return null;
		}

		function deleteCookie(name) {

			var exp = new Date();

			exp.setTime(exp.getTime() - 1);

			var cval = getCookie(name);

			if (cval != null) {

				document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
			}
		}

		return {
			setCookie: setCookie,
			getCookie: getCookie,
			deleteCookie: deleteCookie
		};
	})();
	/**
	 * fn工具函数
	 */
	window.sw.fn = (function() {
		/**
		 * 打印日志文件
		 * @param str
		 */
		function log(str) {
			if (window.console) {
				window.console.log(str);
			}
		}
		return {
			log: log,
			/**
			 * 获取页面url参数值
			 * @param key
			 * @returns {string}
			 */
			getUrlParam: function(key) {

				var value = "",
					itemarr = [],
					urlstr = window.location.search.substr(1);

				if (urlstr) {

					var item = urlstr.split("&");

					for (i = 0; i < item.length; i++) {

						itemarr = item[i].split("=");

						if (key == itemarr[0]) {

							value = itemarr[1];
						}
					}
				}

				return value;
			},

			/**
			 * 判断当前滚动是否到达页面底部
			 * @param min 最小间距
			 * @returns {boolean}
			 */
			pageIsBottom: function(min) {
				//可见区域高度
				var clientHeight = document.documentElement.clientHeight;

				//滚动条高度
				var scrollTop = document.body.scrollTop;

				//当前网页高度
				var scrollHeight = document.body.scrollHeight;

				if (clientHeight + scrollTop + min >= scrollHeight) {
					return true;
				} else {
					return false;
				}
			},
			/**
			 * hack微信中设置title
			 * @param name
			 */
			setTitle: function(name) {
				var $body = $('body');
				document.title = name;
				var $iframe = $('<iframe src="/saowei/favicon.ico"></iframe>').on('load', function() {
					setTimeout(function() {
						$iframe.off('load').remove()
					}, 0)
				}).appendTo($body)
			},
			/**
			 * 计算两点之间距离
			 * @param {Object} d
			 */
			getRad: function(d) {
				return d * Math.PI / 180.0;
			},
			/**
			 * 计算两点之间距离
			 * @param {Object} lat1
			 * @param {Object} lng1
			 * @param {Object} lat2
			 * @param {Object} lng2
			 */
			getFlatternDistance: function(lat1, lng1, lat2, lng2) {
				var EARTH_RADIUS = 6378137.0; //单位M
				var f = sw.fn.getRad((lat1 + lat2) / 2);
				var g = sw.fn.getRad((lat1 - lat2) / 2);
				var l = sw.fn.getRad((lng1 - lng2) / 2);
	
				var sg = Math.sin(g);
				var sl = Math.sin(l);
				var sf = Math.sin(f);
	
				var s, c, w, r, d, h1, h2;
				var a = EARTH_RADIUS;
				var fl = 1 / 298.257;
	
				sg = sg * sg;
				sl = sl * sl;
				sf = sf * sf;
	
				s = sg * (1 - sl) + (1 - sf) * sl;
				c = (1 - sg) * (1 - sl) + sf * sl;
	
				w = Math.atan(Math.sqrt(s / c));
				r = Math.sqrt(s * c) / w;
				d = 2 * w * a;
				h1 = (3 * r - 1) / 2 / c;
				h2 = (3 * r + 1) / 2 / s;
	
				return d * (1 + fl * (h1 * sf * (1 - sg) - h2 * (1 - sf) * sg));
			},
			/*
			 * 生成唯一码openId
			 */
			openId: function() {
				var openid = "";
				for (var i = 1; i <= 32; i++) {
					var n = Math.floor(Math.random() * 16.0).toString(16);
					openid += n;
					if ((i == 8) || (i == 12) | (i == 16) || (i == 20))
						openid += "-";
				}
				return openid;
			},
			/**
			 *  根据功能标示返回样式
			 * @param {Object} fun 
			 */
			getFun: function(fun) {
				if(fun){
					var funArray = {'wifi':'wifi','充电插座':'of'};
			            return funArray[fun];
				}
			}
		};
	})();
	/**
	 * dom操作
	 */
	window.sw.dom = (function() {
		/**
		 * 添加样式到内嵌样式
		 * @param css
		 */
		function addStyle(css) {
			var doc = document,
				head = doc.head;
			var styles = head.getElementsByTagName("style"),
				style;
			if (styles.length == 0) { //如果不存在style元素则创建
				style = doc.createElement('style');
				head.insertBefore(style);
			}
			style = styles[0];
			style.appendChild(doc.createTextNode(css))
		}

		return {
			addStyle: addStyle,

			/**
			 * 获取当前元素（在父元素子节点中）的索引
			 * @param ele
			 * @returns {number}
			 */
			getElementIndex: function(ele) {
				var i = 0;
				while (ele = ele.previousSibling) {
					i++;
				}
				return i;
			}
		};

	})();

	/**
	 * ui模块
	 */
	window.sw.ui = (function() {
		return {
			/**
			 * 单选框
			 */
			radioBox: function(cfg) {
				var self = this;
				this.dialogMaskLayer();
				this.centerBox();
				var centerBox = document.getElementById('centerBox');
				var item_str = '<div id="radioBox" style="border-radius:5px;min-width:281px;">';

				var winHeight = document.documentElement.clientHeight;
				//内容区最大高度
				var maxHeight = winHeight - 48;
				if (cfg.title) { //存在标题
					item_str += '<div style="color:#fff;font-size:15px;padding:10px 0;border-radius:5px 5px 0 0 ;background-color:#592d2d;' +
						'text-align:center;">' + cfg.title + '</div>';
				}

				if (cfg.items) { //存在单选项
					item_str += '<ul style="background-color:#fff;border-radius:0 0 5px 5px;max-height:' + maxHeight + 'px;overflow-y:auto;">';
					for (var i = 0, items = cfg.items, l = items.length; i < l; i++) {
						item_str += '<li style="text-align:center;padding:10px 0;font-size:15px;color:#483131;';
						if (i < l - 1) {
							item_str += 'border-bottom:1px solid #cbcbcb;';
						}
						item_str += '"><span style="position:relative;">' + items[i].value;
						if (cfg.checked == i) { //存在默认选择项
							item_str += '<i class="ui-icon-yes" style="position: absolute;top:4px;right:-20px;"></i>';
						}
						item_str += '</span></li>'
					}
					item_str += '</ul>';
				}

				item_str += '</div>';
				centerBox.innerHTML = item_str;
				document.getElementById('radioBox').addEventListener('click', function(e) {
					//console.log(e.target.tagName);
					var eTarget = e.target;
					while (eTarget.tagName && eTarget.tagName != 'LI') {
						eTarget = eTarget.parentNode;
					}

					if (eTarget.tagName == 'LI') {
						document.getElementById('dialogMaskLayer').style.display = 'none';
						centerBox.style.display = 'none';
						centerBox.removeChild(centerBox.firstChild);
						if (cfg.callback) { //存在回调函数
							var index = sw.dom.getElementIndex(eTarget);
							/**
							 * 元素，元素索引
							 */
							cfg.callback(cfg.items[index], index);
						}
					}
					e.stopPropagation();
				}, false);
				document.getElementById('centerBox').addEventListener('click', self.hideMaskLayer, false);
			},

			buttonEvent: null,

			/**
			 * 提示框
			 * @param cfg
			 * cfg.content 提示内容
			 * cfg.callback 回调函数
			 */
			tooltip: function(cfg) {
				var self = this;
				this.dialogMaskLayer();
				this.centerBox();
				var centerBox = document.getElementById('centerBox');
				var inner = '<div style="width:260px;text-align: center;padding:10px;background-color:rgba(0,0,0,0.7);color:#f2f2f2;font-size:14px;border-radius:5px;">' +
					cfg.content +
					'</div>';
				centerBox.innerHTML = inner;

				if (self.tooltipTimer) {
					clearTimeout(self.tooltipTimer);
				}
				self.tooltipTimer = setTimeout(function() {
					self.hideMaskLayer();
					cfg.callback && cfg.callback();
					//console.log('aaa');
				}, 3000);
				// self.closeMaskLayer();
			},
			/**
			 * 对话框
			 * @param cfg
			 */
			dialog: function(cfg) {
				var self = this;
				if (cfg == 'close') {
					if (self.buttonEvent) {
						document.removeEventListener('click', self.buttonEvent, false);
					}
					self.hideMaskLayer();
					return;
				}
				this.dialogMaskLayer();
				this.centerBox();
				var centerBox = document.getElementById('centerBox');
				//内容区最大高度
				var maxHeight = 0;
				var winHeight = document.documentElement.clientHeight;
				if (cfg.type == 1 || cfg.type == 2) {
					maxHeight = winHeight - 125;
					if (self.buttonEvent) {
						document.removeEventListener('click', self.buttonEvent, false);
					}
					cfg.labelOk = cfg.labelOk ? cfg.labelOk : '确认';
					cfg.labelCancel = cfg.labelCancel ? cfg.labelCancel : '取消';
					var panel, btnstr;
					btnstr = (cfg.type == 1) ? '<span id="sw_btn_ok" style="border-radius:0 0 4px 4px;" onclick="">' + cfg.labelOk + '</span>' : '<span onclick="" id="sw_btn_cancel" style="border-right:1px solid #adadad;border-radius:0 0 0 4px;">' + cfg.labelCancel + '</span><span style="border-radius:0 0 4px 0;" id="sw_btn_ok" onclick="">' + cfg.labelOk + '</span>';
					var swDialog = document.createElement('div');
					swDialog.id = 'sw_dialog';
					swDialog.className = 'sw-dialog';

					panel = '<div id="closeDialog" class="title">' + cfg.title + '</div><div class="content" style="max-height:' + maxHeight + 'px;overflow-y:auto;">' + cfg.content + '</div><div class="button">' + btnstr + '</div>';
					swDialog.innerHTML = panel;
					//移除重复对话框
					var swOldDialog = null;
					if (swOldDialog = document.getElementById('sw_dialog')) {
						swOldDialog.parentNode.removeChild(swOldDialog);
					}
					centerBox.appendChild(swDialog);
					self.buttonEvent = function(e) {
						if (e.target == document.getElementById('sw_btn_ok')) { //确认
							cfg.callbackOk && cfg.callbackOk();
							e.stopPropagation();
						} else if (e.target == document.getElementById('sw_btn_cancel')) { //取消
							cfg.callbackCancel && cfg.callbackCancel();
							e.stopPropagation();
						}
					}

					document.addEventListener('click', self.buttonEvent, false);
				} else if (cfg.type == 3) { //自定义弹出框内容
					centerBox.innerHTML = cfg.innerhtml;
					cfg.callback && cfg.callback();
				}
				//self.closeMaskLayer();
			},

			closeMaskEvent: null,

			/**
			 * 关闭遮罩
			 */
			closeMaskLayer: function() {
				var self = this;
				var closeDialog = document.getElementById('closeDialog');
				if (closeDialog) {
					closeDialog.addEventListener('click', function() {
						self.hideMaskLayer();
					}, false);
				}
				if (self.closeMaskEvent) {
					document.removeEventListener('click', self.closeMaskEvent, false);
				}
				self.closeMaskEvent = function(e) {
					if (e.target.tagName && e.target.id == 'centerBox') {
						self.hideMaskLayer();
					}
				}

				document.addEventListener('click', self.closeMaskEvent, false);
			},


			/**
			 * 隐藏遮罩和弹性盒子
			 */
			hideMaskLayer: function() {
				var self = this;
				if (self.tooltipTimer) {
					clearTimeout(self.tooltipTimer);
				}
				var centerBox = document.getElementById('centerBox');
				document.getElementById('dialogMaskLayer').style.display = 'none';
				centerBox.style.display = 'none';
				var children = centerBox.childNodes;
				for (var i = 0, l = children.length; i < l; i++) {
					centerBox.removeChild(centerBox.firstChild);
				}
				document.documentElement.style.overflowY = 'visible';
			},

			/**
			 * 遮罩
			 */
			dialogMaskLayer: function() {
				var dialogMaskLayer = null;
				//存在遮罩了，这时候只要做显示/隐藏操作
				if (dialogMaskLayer = document.getElementById('dialogMaskLayer')) {
					dialogMaskLayer.style.display = 'block';
				} else {
					dialogMaskLayer = document.createElement('div');
					dialogMaskLayer.id = 'dialogMaskLayer';
					dialogMaskLayer.style.cssText = 'width:100%;height:100%;position:fixed;z-index:1000;' +
						'top:0;left:0;background-color:rgba(0,0,0,0.6);';
					document.documentElement.style.overflowY = 'hidden';
					document.body.appendChild(dialogMaskLayer);
				}
			},
			/**
			 * 水平垂直居中弹性盒子
			 */
			centerBox: function() {
				var centerBox = null;
				if (centerBox = document.getElementById('centerBox')) {
					centerBox.style.display = '-webkit-box';
				} else {
					centerBox = document.createElement('div');
					centerBox.id = 'centerBox';
					centerBox.setAttribute('onclick', '');
					centerBox.style.cssText = 'width:100%;height:100%;position:fixed;z-index:1001;' +
						'top:0;left:0;display:box;display:-webkit-box;box-align:center;-webkit-box-align:center;' +
						'box-pack:center;-webkit-box-pack:center;box-orient:vertical;-webkit-box-orient:vertical;';
					centerBox.addEventListener('touchstart', function(e) {
						e.stopPropagation();
					}, false);
					centerBox.addEventListener('touchmove', function(e) {
						e.stopPropagation();
					}, false);
					centerBox.addEventListener('touchend', function(e) {
						e.stopPropagation();
					}, false);
					document.body.appendChild(centerBox);
				}
			},

			/**
			 * 加载进度条
			 * @param cfg
			 */
			loading: function(cfg) {
				if (cfg == 'hide') {
					document.getElementById('maskLayer').style.display = 'none';
				} else {
					if (!document.getElementById('maskLayer')) {
						var doc = document,
							head = doc.head,
							body = doc.body;
						addStyle('@-webkit-keyframes mask-layer-box-circle-key{from { -webkit-transform:rotate(0) }to{ -webkit-transform:rotate(360deg)}}' +
							'.mask-layer{display:none;position: fixed;width:100%;height:100%;top:0;left:0;background:#000;z-index:1000;opacity:0.5}' +
							'.mask-layer-box{font-size:14px;font-family:"Microsoft YaHei";width:150px;height:100px;position:absolute;border-radius:8px;color:#fff;text-align:center;top:50%;left:50%;margin-top:-50px;margin-left:-75px;}' +
							'.mask-layer-box-circle{width:30px;height:30px;border-radius:30px;margin:10px auto 20px;border:3px solid #ddd;border-top-color:transparent;border-right-color:transparent;box-shadow:0 0 6px #333;-webkit-animation:mask-layer-box-circle-key 1s linear 0 infinite;}');

						var maskLayer = doc.createElement('div');
						maskLayer.id = 'maskLayer';
						maskLayer.className = 'mask-layer';
						body.appendChild(maskLayer);
						maskLayer.innerHTML = '<div class="mask-layer-box"><div class="mask-layer-box-circle"></div>客官, 请稍等~</div>';

						/**
						 * 添加样式到内嵌样式
						 * @param css
						 */
						function addStyle(css) {
							var styles = head.getElementsByTagName("style"),
								style;
							if (styles.length == 0) { //如果不存在style元素则创建
								style = doc.createElement('style');
								head.insertBefore(style);
							}
							style = styles[0];
							style.appendChild(doc.createTextNode(css))
						}
					}
					document.getElementById('maskLayer').style.display = 'block';
				}
			}
		};
	})();

	//ui-button active hack
	var uiButtons = document.getElementsByClassName('ui-button');
	for (var i = 0; i < uiButtons.length; i++) {
		uiButtons[i].addEventListener('touchstart', function() {}, false);
	}
})(window, document, undefined);